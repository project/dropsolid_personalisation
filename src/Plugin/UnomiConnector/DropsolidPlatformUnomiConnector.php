<?php

namespace Drupal\dropsolid_personalisation\Plugin\UnomiConnector;

use Dropsolid\OAuth2\Client\Http\Guzzle\Middleware\RefreshTokenMiddleware;
use Dropsolid\UnomiSdkPhp\Http\ApiClient\ApiClient;
use Dropsolid\OAuth2\Client\Provider\DropsolidPlatform;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dropsolid_personalisation\DropsolidPersonalisationException;
use Drupal\unomi\UnomiConnector\UnomiConnectorPluginBase;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use Http\Adapter\Guzzle6\Client;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Dropsolid Platform Unomi connector.
 *
 * @UnomiConnector(
 *   id = "dropsolid_platform",
 *   label = @Translation("Dropsolid Platform"),
 *   description = @Translation("A Dropsolid Platform connector usable for the saas services of the Dropsolid Platform.")
 * )
 */
class DropsolidPlatformUnomiConnector extends UnomiConnectorPluginBase {

  /**
   * Drupal state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition) {
    $configuration += $this->defaultConfiguration();
    $this->state = \Drupal::service('state');
    $this->settings = \Drupal::service('settings');

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'client_id' => '',
      'client_secret' => '',
      'scheme' => 'https',
      'port' => 443,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Dropsolid Personalization Integration settings'),
      '#description' => $this->t(
        'To retrieve your client ID & secret: <ol>
          <li>Visit <a href="@url">Dropsolid Platform</a> and create a new keypair.</li>
          <li>Retrieve your client ID/Secret under the "OAuth2 credentials" header.</li>
          </ol>',
        [
          '@url' => 'https://platform.dropsolid.com',
        ]
      ),
      '#collapsible' => TRUE,
      '#collapsed' => empty($this->configuration['client_id']),
    ];

    $form['auth']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $this->configuration['client_id'],
      '#required' => TRUE,
    ];

    $form['auth']['client_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $this->configuration['client_secret'],
      '#description' => $this->t('If this field is left blank and the Client Secret was previously filled out, the current Client Secret will not be changed.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Since the form is nested into another, we can't simply use #parents for
    // doing this array restructuring magic. (At least not without creating an
    // unnecessary dependency on internal implementation.)
    // For password fields, there is no default value, they're empty by default.
    // Therefore we ignore empty submissions if the user didn't change either.
    if ($values['auth']['client_secret'] === ''
      && isset($this->configuration['client_id'])
      && $values['auth']['client_id'] === $this->configuration['client_id']) {
      // Set the client_secret even though it wasn't filled in.
      $values['auth']['client_secret'] = $this->configuration['client_secret'];
    }

    foreach ($values['auth'] as $key => $value) {
      $form_state->setValue($key, $value);
    }

    // Clean-up the form to avoid redundant entries in the stored configuration.
    $form_state->unsetValue('auth');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $vars = [
      '%client_id' => $this->configuration['client_id'],
      '%client_secret' => str_repeat('*', strlen($this->configuration['client_secret'])),
    ];

    $info[] = [
      'label' => $this->t('Dropsolid Platform Oauth Credentials'),
      'info' => $this->t('Client ID: %client_id | Client Secret: %client_secret', $vars),
    ];

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiClient() {
    try {
      // Create provider.
      $provider = $this->createDropsolidPlatformProvider();
    }
    catch (\Exception $e) {
      \Drupal::logger('dropsolid_personalisation')->error('Error creating an API Client: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }

    // Get the JsonSerialized version of an access token.
    $accessToken = $this->state->get('dropsolid_platform_access_token', '');
    if (!$accessToken) {
      try {
        $accessToken = $provider->getAccessToken('client_credentials')->jsonSerialize();
      }
      catch (\Exception $e) {
        \Drupal::logger('dropsolid_personalisation')->error('Error fetching an access token: @message', ['@message' => $e->getMessage()]);
        return NULL;
      }
    }
    return self::createOauthApiClient(
      $provider,
      new AccessToken($accessToken),
      [
        'callback' => [$this, 'accessToken'],
        // @todo replace this with the url from the config
        'base_uri' => $this->getServerUri(),
      ]
    );
  }

  /**
   * Create OAuth Guzzle Provider ApiClient for Unomi.
   *
   * @param \League\OAuth2\Client\Provider\AbstractProvider $provider
   *   The provider that will take care of the authentication layer such as
   *   Basic Auth or OAuth client_credentials.
   * @param \League\OAuth2\Client\Token\AccessTokenInterface|array $accessToken
   *   The access token object.
   * @param array $config
   *   Additional configuration for this client.
   *
   * @return \Dropsolid\UnomiSdkPhp\Http\ApiClient\ApiClient
   *   The Unomi Api Client.
   */
  public static function createOauthApiClient(
    AbstractProvider $provider,
    AccessTokenInterface $accessToken,
    array $config = []
  ) {
    if (!isset($config['handler'])) {
      if (!isset($config['callback'])) {
        $config['callback'] = NULL;
      }
      $handler = self::initialiseHandlerStack(
        $provider,
        $accessToken,
        $config['callback']
      );

      $config['handler'] = $handler;
    }
    if (!isset($config['options'])) {
      $config['options'] = [];
    }

    $httpClient = new GuzzleClient($config);
    $psrClient = new Client($httpClient);

    return new ApiClient($provider, $psrClient, $accessToken, $config);
  }

  /**
   * Helper function to setup a handler stack.
   *
   * This function adds the refreshTokenMiddleware to the handlerstack
   * so that on an invalid token, it tries to refresh it, helping the clients
   * to not implement this logic over and over.
   *
   * @param \League\OAuth2\Client\Provider\AbstractProvider $provider
   *   The provider that will take care of the authentication layer such as
   *   Basic Auth or OAuth client_credentials.
   * @param \League\OAuth2\Client\Token\AccessTokenInterface $accessToken
   *   The access token object.
   * @param callable $refreshTokenCallback
   *   The function that should take care of refreshing the token.
   *
   * @return \GuzzleHttp\HandlerStack
   *   The Guzzle Handlerstack.
   */
  public static function initialiseHandlerStack(
    AbstractProvider $provider,
    AccessTokenInterface $accessToken,
    callable $refreshTokenCallback = NULL
  ) {
    $handlerStack = HandlerStack::create();
    $refreshMiddleware = new RefreshTokenMiddleware(
      $provider,
      $accessToken,
      $refreshTokenCallback
    );
    $handlerStack->push($refreshMiddleware);

    return $handlerStack;
  }

  /**
   * Helper function to grab the access token from the Drupal state system.
   */
  public function accessToken(AccessToken $token) {
    $this->state->set('dropsolid_platform_access_token', $token->jsonSerialize());
  }

  /**
   * Create a new Dropsolid Platform OAuth2 Provider based on module config.
   *
   * @return \Dropsolid\OAuth2\Client\Provider\DropsolidPlatform
   *   The OAuth2 Provider object
   *
   * @throws \Drupal\dropsolid_personalisation\DropsolidPersonalisationException
   */
  protected function createDropsolidPlatformProvider() {
    // Find out if we overrode our oauth path to get our tokens from.
    // Useful when working on the QA environment of Dropsolid Platform.
    $platformOauthSettings = $this->settings::get('dropsolid_platform',
      [
        'oauth' => [
          'urlAuthorize' => 'https://admin.platform.dropsolid.com/oauth/authorize',
          'urlAccessToken' => 'https://admin.platform.dropsolid.com/oauth/token',
          'urlResourceOwnerDetails' => 'https://admin.platform.dropsolid.com/oauth/user.info',
        ],
      ]
    );

    $urlAuthorize = (isset($platformOauthSettings['oauth']['urlAuthorize'])) ? $platformOauthSettings['oauth']['urlAuthorize'] : 'https://admin.platform.dropsolid.com/oauth/authorize';
    $urlAccessToken = (isset($platformOauthSettings['oauth']['urlAccessToken'])) ? $platformOauthSettings['oauth']['urlAccessToken'] : 'https://admin.platform.dropsolid.com/oauth/token';
    $urlResourceOwnerDetails = (isset($platformOauthSettings['oauth']['urlResourceOwnerDetails'])) ? $platformOauthSettings['oauth']['urlResourceOwnerDetails'] : 'https://admin.platform.dropsolid.com/oauth/user.info';

    if (empty($this->configuration['client_id']) || empty($this->configuration['client_secret'])) {
      throw new DropsolidPersonalisationException('The client_id or client_secret is empty');
    }

    return new DropsolidPlatform([
      // The client ID assigned to you by the provider.
      'clientId'                => $this->configuration['client_id'],
      // The client password assigned to you by the provider.
      'clientSecret'            => $this->configuration['client_secret'],
      'urlAuthorize'            => $urlAuthorize,
      'urlAccessToken'          => $urlAccessToken,
      'urlResourceOwnerDetails' => $urlResourceOwnerDetails,
      'scopes' => 'cdp_admin',
    ]);
  }

}
