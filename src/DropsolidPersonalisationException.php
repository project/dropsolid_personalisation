<?php

namespace Drupal\dropsolid_personalisation;

/**
 * Represents an Dropsolid Personalisation exception.
 */
class DropsolidPersonalisationException extends \Exception {}
